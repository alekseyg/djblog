from django.conf.urls import patterns, url


urlpatterns = patterns('blog.api',
    url(r'^blog/post/list/$', 'post_list', name='api_post_list'),
    url(r'^blog/post/details/$', 'post_details', name='api_post_details'),
    url(r'^blog/post/new/$', 'new_post', name='api_post_new'),
    url(r'^blog/post/edit/$', 'edit_post', name='api_post_edit'),
    url(r'^blog/post/delete/$', 'delete_post', name='api_post_delete'),
)
