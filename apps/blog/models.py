from django.db import models


class Post(models.Model):

    title = models.CharField(max_length=256)
    description = models.TextField(null=True, blank=True)
    content = models.TextField()

    created = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u"%s" % self.title

    def to_dict(self):
        return {
            'id': self.id,
            'title': self.title,
            'description': self.description,
            'content': self.content,
            'created': self.created,
            'last_update': self.last_update
        }
