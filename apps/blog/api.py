from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST


from blog.models import Post
from blog.forms import PostForm


@csrf_exempt
def post_list(request):

    posts = [post.to_dict() for post in Post.objects.all()]

    return JsonResponse({
        'success': True,
        'posts': posts
    })


@csrf_exempt
def post_details(request):

    post = get_object_or_404(Post, pk=request.GET.get("post_id"))

    return JsonResponse({
        'success': True,
        'post': post.to_dict()
    })


@csrf_exempt
@require_POST
def new_post(request):

    form = PostForm(request.POST)

    if form.is_valid():
        post = form.save()

        response = {
            'success': True,
            'post': post.to_dict()
        }

    else:
        response = {
            'success': False,
            'errors': form.errors
        }

    return JsonResponse(response)


@csrf_exempt
@require_POST
def edit_post(request):

    post = get_object_or_404(Post, pk=request.POST.get("post_id"))
    form = PostForm(request.POST, instance=post)

    if form.is_valid():
        post = form.save()

        response = {
            'success': True,
            'post': post.to_dict()
        }

    else:
        response = {
            'success': False,
            'errors': form.errors
        }

    return JsonResponse(response)


@csrf_exempt
@require_POST
def delete_post(request):

    post = get_object_or_404(Post, pk=request.POST.get("post_id"))
    post.delete()

    return JsonResponse({
        'success': True
    })
