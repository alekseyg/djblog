Djblog.Posts = ->
  self = riot.observable(this)
  backend = new Djblog.Backend()


  addOrEdit = (action, params) ->
    backend.call action, params, (response) ->
      if response.success
        self.trigger action, response.post
      else
        self.trigger 'formError', response.errors


  self.add = (params) -> addOrEdit('add', params)
  self.edit = (params) -> addOrEdit('edit', params)


  self.list = ->
    backend.call 'list', {}, (response) ->
      if response.success
        posts = $.map response.posts, (post) ->
          $.extend post, {
            created_fm: Djblog.format_time(post.created)
          }
        self.trigger 'list', posts
      else
        self.trigger 'error', 'Error getting post list'


  self.destroy = (postId) ->
    backend.call 'destroy', {post_id: postId}, (response) ->
      if response.success
        self.trigger 'destroy', postId
      else
        self.trigger 'error', 'Error deleting post'


  self.details = (postId) ->
    backend.call 'details', {post_id: postId}, (response) ->
      if response.success
        post = $.extend response.post, {
          created_fm: Djblog.format_time(response.post.created),
          content_md: marked(response.post.content)
        }
        self.trigger 'details', post
      else
        self.trigger 'error', 'Error getting post details'


  self
