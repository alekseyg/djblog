initPosts = ->
  posts = new Djblog.Posts()
  Djblog.presenter(posts)
  Djblog.initUI(posts)

  riot.route (path) ->
    posts.trigger 'load', path.slice(2)


$ initPosts
