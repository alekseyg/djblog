Djblog.presenter = (model) ->
  listTemplate = $('#list-template').html()
  detailsTemplate = $('#details-template').html()
  formTemplate = $('#form-template').html()
  errorTemplate = $('#error-template').html()
  currentView = ''
  firstLoad = true


  capitalize = (s) -> s.charAt(0).toUpperCase() + s.slice(1)


  model.on 'load', (path) ->
    $('body').addClass('loading').removeClass(currentView)
    currentView = path

    if firstLoad || path == ''
      model.list()
      firstLoad = false

    parts = path.split('/')

    switch parts[0]
      when ''
        currentView = 'list'
      when 'new', 'edit'
        post = {action: capitalize(parts[0])}

        if parts[0] == 'edit'
          post = $.extend post, $('#details-view').data('post')

        $form = $(riot.render(formTemplate, post))
        $('#form-view').html($form)
        model.trigger 'loaded'

        currentView = 'form'
      when 'post'
        model.details parts[1]
        currentView = 'details'

    $('body').addClass(currentView)
    highlightLinks(path)


  highlightLinks = (path) ->
    $('a[href^="#/"].active').removeClass('active')
      .parent('li').removeClass('active')
    $("a[href='#/#{path}']").addClass('active')
      .parent('li').addClass('active')


  model.on 'loaded', ->
    $('body').removeClass('loading')
    $('.timeago').timeago()


  addPost = (post) ->
    $post = $(riot.render(listTemplate, post))
    $('#posts-list').prepend($post)
    model.trigger 'loaded'


  postUrl = (post) -> "#/post/#{post.id}"


  model.on 'add', (post) ->
    addPost post
    riot.route postUrl(post)


  model.on 'edit', (post) ->
    $post = $(riot.render(listTemplate, post))
    postHref = postUrl(post)
    $('#posts-list').find("[href='#{postHref}']")
      .replaceWith($post)
    riot.route postUrl(post)


  model.on 'list', (posts) ->
    $('#posts-list').html('')
    $.each posts, ->
      addPost this
    model.trigger 'loaded'
    highlightLinks(location.hash.slice(2))


  model.on 'destroy', (postId) ->
    $("#post-#{postId}").remove()
    model.trigger 'loaded'
    riot.route '#/'


  model.on 'details', (post) ->
    $post = $(riot.render(detailsTemplate, post))
    $('#details-view').html($post).data('post', post)
    model.trigger 'loaded'


  model.on 'error', (message) ->
    $message = $(riot.render(errorTemplate, {message: message}))
    $('#notifications').append($message)
    model.trigger 'loaded'


  model.on 'formError', (errors) ->
    model.trigger 'error', 'Please review the problems below'

    $.each errors, (name, messages) ->
      $field = $("[name='#{name}']")
      $messages = $('<ul class="help-block errors" />')
      $.each messages, ->
        $messages.append $("<li>#{this}</li>")

      $field.closest('.form-group')
        .addClass('has-error')
        .append($messages)
