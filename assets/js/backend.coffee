Djblog.Backend = ->
  self = this

  api =
    add:
      type: 'POST'
      url: '/api/blog/post/new/'
    list:
      type: 'GET'
      url: '/api/blog/post/list/'
    destroy:
      type: 'POST'
      url: '/api/blog/post/delete/'
    details:
      type: 'GET'
      url: '/api/blog/post/details/'
    edit:
      type: 'POST'
      url: '/api/blog/post/edit/'

  self.call = (action, data, fn) ->
    if action = api[action]
      $.ajax($.extend(action, {data: data, success: fn}))
    else
      throw 'Unsupported API request'

  self
