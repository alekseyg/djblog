Djblog.initUI = (model) ->
  $('body').on 'click', '[href^="#/"]', (e) ->
    riot.route $(this).attr('href')

  $('body').on 'submit', '#post-form', (e) ->
    e.preventDefault()

    $form = $(this)
    params = {}

    $form.find('.errors').remove()
    $form.find('.has-error').removeClass('has-error')

    $.each $form.serializeArray(), ->
      params[this.name] = this.value

    if params.post_id
      model.edit(params)
    else
      model.add(params)

    $('body').addClass('loading')


  $('body').on 'click', '.delete-post', (e) ->
    e.preventDefault()

    if confirm('Are you sure you want to delete this post?')
      postId = $(this).data('id')
      model.destroy(postId)


Djblog.format_time = (time) ->
  time = new Date(time)

  "#{time.toLocaleDateString()} at #{time.toLocaleTimeString()}"
