var gulp = require('gulp');

var less      = require('gulp-less');
var minifycss = require('gulp-minify-css');

var gulpif = require('gulp-if');
var coffee = require('gulp-coffee');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

var notify     = require('gulp-notify');
var clean      = require('gulp-clean');
var livereload = require('gulp-livereload');
var lr         = require('tiny-lr');
var server     = lr();

gulp.task('less', function() {
    return gulp.src('assets/less/style.less')
        .pipe(less().on('error', notify.onError(function (error) {
            return 'Error compiling LESS: ' + error.message;
        })))
        .pipe(minifycss())
        .pipe(gulp.dest('static/css'))
        .pipe(livereload(server))
        .pipe(notify({
            message: 'Successfully compiled LESS'
        }));
});

gulp.task('lint', function() {
    return gulp.src('assets/js/script.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(livereload(server));
});

gulp.task('js', function() {
    return gulp.src([
            'bower_components/jquery/dist/jquery.js',
            'bower_components/riotjs/riot.js',
            'bower_components/bootstrap/dist/js/bootstrap.js',
            'bower_components/marked/lib/marked.js',
            'bower_components/timeago/jquery.timeago.js',
            'assets/js/init.coffee',
            'assets/js/backend.coffee',
            'assets/js/model.coffee',
            'assets/js/ui.coffee',
            'assets/js/presenter.coffee',
            'assets/js/app.coffee'
        ])
        .pipe(gulpif(/[.]coffee$/, coffee()))
        .pipe(concat('script.js'))
        .pipe(uglify())
        .pipe(gulp.dest('static/js'))
        .pipe(livereload(server))
        .pipe(notify({
            message: 'Successfully compiled JS'
        }));
});

// Clean
gulp.task('clean', function() {
    return gulp.src(['static/css', 'static/js'], {read: false})
        .pipe(clean());
});

// Default task
gulp.task('default', ['clean'], function() {
    gulp.start('less', 'lint', 'js');
});

// Watch
gulp.task('watch', function() {
    // Listen on port 35729
    server.listen(35729, function (err) {
        if (err) {
            return console.log(err);
        }

        // Watch .less files
        gulp.watch('less/**/*.less', ['less']);

        // Watch .js files
        gulp.watch('js/**/*.js', ['lint', 'js']);
    });
});
