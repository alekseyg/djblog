In order to setup and run the djblog project, extract and execute this commands in the djblog's source folder:


1. Install virtualenv
sudo apt-get install python-virtualenv

2. Install pip
sudo apt-get install python-pip

3. Create a new virtualenv
virtualenv env

4. Activate the new environment
source env/bin/activate

5. Install requirements
pip install -r requirements.txt

6. Syncdb
python manage.py migrate

7. Create admin
python manage.py createsuperuser

8. Run server
python manage.py runserver


In order to recompile the assets, you will need to install node.js and npm

1. Install node.js for your platform, e.g. for Ubuntu
sudo apt-get install nodejs

2. Install npm. On Mac and Windows, node.js comes with npm. For Ubuntu
sudo apt-get install npm

3. Install node dependencies
npm install

4. Install bower dependencies
bower install

5. Compile assets with gulp
gulp
